# Software Engineering Test #

The purpose of the test is to build a console application that scrapes the grocery site - specific product page and returns a JSON array of all the products on the page.

### Requirements ###

* Java 8
* Maven 3

### Running the project ###

Build jar file:
```
mvn clean compile assembly:single
```

Run application:
```
java -jar target/scraper-0.1-jar-with-dependencies.jar
```

Run tests:
```
mvn test
```

### Application dependencies ###

* [jsoup](http://jsoup.org/) - Scrapes, parses, manipulates and cleans HTML.
* [Genson](http://owlike.github.io/genson/) - Powerful and easy to use Java to JSON conversion library.
* [config](https://github.com/typesafehub/config) - Configuration library for JVM languages.
* [JUnit](http://junit.org) - Common testing framework.