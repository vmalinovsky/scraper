package com.company;

import com.owlike.genson.annotation.JsonProperty;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.URL;

public class Product {
    private static final String PRICE_SELECTOR = ".pricePerUnit";
    private static final String TITLE_SELECTOR = ".productInfo a";
    private static final String DESCRIPTION_SELECTOR = ".productText p";

    private float price;
    private String title;
    private String description;

    private final URL customUrl;

    private Resource detailsResource;

    public static class Builder {
        private final Element element;
        private URL customUrl;

        public Builder(Element element) {
            this.element = element;
        }

        public Builder customProductDetailsUrl(URL customUrl) {
            this.customUrl = customUrl;
            return this;
        }

        public Product build() throws IOException {
            return new Product(this);
        }
    }

    private Product(Builder builder) throws IOException {
        customUrl = builder.customUrl;
        parseProductElement(builder.element);
    }

    private void parseProductElement(Element element) throws IOException {
        price = Float.parseFloat(element.select(PRICE_SELECTOR).first().ownText().replace("&pound", ""));
        title = element.select(TITLE_SELECTOR).text();

        URL url;
        if (customUrl != null) {
            url = customUrl;
        } else {
            url = new URL(element.select(TITLE_SELECTOR).attr("href"));
        }

        detailsResource = new Resource(url);
        description = detailsResource.getDocument().select(DESCRIPTION_SELECTOR).first().text();
    }

    // The following getters are implicitly used by genson library to generate json.

    @JsonProperty("unit_price")
    public float getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    @JsonProperty("size")
    public String getPageSizePretty() {
        return detailsResource.getSizePretty();
    }
}