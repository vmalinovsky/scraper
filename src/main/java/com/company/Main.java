package com.company;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) throws IOException {
        Config conf = ConfigFactory.load();
        URL url = new URL(conf.getString("product-list-url"));

        Scraper scraper = new Scraper.Builder(url).build();

        Map<String, Object> result = new HashMap<>();
        result.put("results", scraper.getProducts());
        result.put("total", (float) scraper.getTotal());

        Genson genson = new GensonBuilder()
                .useIndentation(true)
                .create();

        System.out.println(genson.serialize(result));
    }
}
