package com.company;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

public class Resource {
    private static final String RESOURCE_SIZE_FORMAT = "#.#kb";

    private Document document;
    private int size = 0;

    public Resource(URL url) throws IOException {
        URLConnection con = url.openConnection();

        size = con.getContentLength();
        document = Jsoup.parse(con.getInputStream(), con.getContentEncoding(), url.getPath());
    }

    public Document getDocument() {
        return document;
    }

    public int getSize() {
        return size;
    }

    public String getSizePretty() {
        return new DecimalFormat(RESOURCE_SIZE_FORMAT).format(size / 1024F);
    }
}
