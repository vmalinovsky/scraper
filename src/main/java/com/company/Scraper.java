package com.company;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class Scraper {
    private static final String PRODUCT_SELECTOR = ".product";

    private final ArrayList<Product> products = new ArrayList<>();

    public static class Builder {
        private final URL url;
        private URL customProductDetailsUrl;

        public Builder(URL url) {
            this.url = url;
        }

        public Builder customProductDetailsUrl(URL customUrl) {
            this.customProductDetailsUrl = customUrl;
            return this;
        }

        public Scraper build() throws IOException {
            return new Scraper(this);
        }
    }

    private Scraper(Builder builder) throws IOException {
        Document doc = new Resource(builder.url).getDocument();
        Elements elements = doc.select(PRODUCT_SELECTOR);

        for (Element element : elements) {
            products.add(new Product.Builder(element)
                    .customProductDetailsUrl(builder.customProductDetailsUrl)
                    .build());
        }
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public double getTotal() {
        return products
                .stream()
                .mapToDouble(Product::getPrice)
                .sum();
    }
}
