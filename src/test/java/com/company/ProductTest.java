package com.company;

import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;

public class ProductTest extends BaseTest {

    private static Product product;

    @BeforeClass
    public static void loadProductData() throws IOException {
        Element element = new Resource(productListURL)
                .getDocument()
                .select(".product")
                .first();

        product = new Product.Builder(element)
                .customProductDetailsUrl(productDetailsURL)
                .build();
    }

    @Test
    public void testProductTitle() throws Exception {
        Assert.assertEquals("Sainsbury's Apricot Ripe & Ready x5", product.getTitle());
    }

    @Test
    public void testProductPrice() {
        Assert.assertEquals(3.50, product.getPrice(), DELTA);
    }

    @Test
    public void testProductDescription() throws Exception {
        Assert.assertEquals("Apricots", product.getDescription());
    }

    @Test
    public void testPageSizePretty() {
        Assert.assertEquals("43kb", product.getPageSizePretty());
    }
}