package com.company;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ResourceTest extends BaseTest {

    private static Resource resource;

    @BeforeClass
    public static void init() throws Exception {
        resource = new Resource(productDetailsURL);
    }

    @Test
    public void testDocumentNotNull() throws Exception {
        Assert.assertNotNull(resource.getDocument());
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(44062, resource.getSize());
    }

    @Test
    public void testGetSizePretty() throws Exception {
        Assert.assertEquals("43kb", resource.getSizePretty());
    }
}