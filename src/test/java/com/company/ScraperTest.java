package com.company;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

public class ScraperTest extends BaseTest {

    private static Scraper scraper;

    @BeforeClass
    public static void init() throws IOException {
        scraper = new Scraper.Builder(productListURL)
                .customProductDetailsUrl(productDetailsURL)
                .build();
    }

    @Test
    public void testGetTotal() {
        Assert.assertEquals(15.10F, (float) scraper.getTotal(), DELTA);
    }

    @Test
    public void testGetProducts() throws Exception {
        Assert.assertEquals(7, scraper.getProducts().size());
    }
}