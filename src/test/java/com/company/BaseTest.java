package com.company;

import java.net.URL;

public class BaseTest {

    protected static final URL productListURL = getSampleURL("product_list.html");
    protected static final URL productDetailsURL = getSampleURL("product_details.html");

    protected static final double DELTA = 1e-15;

    private static URL getSampleURL(String fileName) {
        return Thread
                .currentThread()
                .getContextClassLoader()
                .getResource(fileName);
    }
}
